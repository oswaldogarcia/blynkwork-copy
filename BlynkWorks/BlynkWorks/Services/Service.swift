//
//  Service.swift
//  BlynkWorks
//
//  Created by Shirley on 19/2/18.
//  Copyright © 2018 Teravision. All rights reserved.
//

import Foundation

protocol Service{
    
    func callServiceObject(parameters:[String: AnyObject]?,service:String, withCompletionBlock: @escaping  ((AnyObject?, _ error: NSError?) -> Void))
    
}
