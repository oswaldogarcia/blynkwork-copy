//
//  ShellWebService.swift
//  BlynkWorks
//
//  Created by Shirley on 19/2/18.
//  Copyright © 2018 Teravision. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import KSToastView
import KVNProgress

class ShellWebService : Service {
    
    var currentId = 0
    
    func selectWebService(service:String, params:[String:AnyObject]?, returnService: ((_ method: HTTPMethod, _ serviceUrl: String?, _ typeEncoding: ParameterEncoding) -> Void)){
        
        tokenRefreshBool = true
        
        switch service {
        case GlobalConstants.nameServices.accountRegister:
            returnService(.post,GlobalConstants.Endpoints.accountRegisterWS, JSONEncoding())
            tokenRefreshBool = false
            break
        default:
            break
        }
        
    }
    
    func editParameters(parameters: [String : AnyObject]?)->[String : AnyObject]?{
        
        var parametersEdited:[String : AnyObject] = [:]
        if parameters != nil{
            parametersEdited = parameters!
        }
        
        return parametersEdited
    }
    
    func settingHeader()->[String : String]?{
        
        var header:[String : String] = ["Content-Type": GlobalConstants.Headers.contentType, "Accept":GlobalConstants.Headers.contentType]
        
        if(self.tokenRefreshBool){
            if let token = (UKPSessionHelper.getSession(key: GlobalConstants.Keys.token) as? String) {
                header["Authorization"] = "Bearer " + token
            }
        }
        
        return header
    }
    
    
    func callServiceObject(parameters:[String: AnyObject]?,service:String, withCompletionBlock: @escaping ((AnyObject?, _ error: NSError?) -> Void)){
        
        selectWebService(service: service, params:parameters,  returnService: { (method, url, typeEncoding) -> Void in
            
            let headers = settingHeader()
            let parametersEdited = self.editParameters(parameters: parameters)
            
            print("----------API SERVICE-------------")
            print(url ?? "nil")
            print(parametersEdited ?? "nil params")
            print(headers ?? "nil headers")
            print("----------------------------------")
            

            Alamofire.request(url!, method: method, parameters: parametersEdited, encoding: typeEncoding, headers:headers)
                .responseObject{ (response: DataResponse<JSONResponse>) in
                    print(response.response?.statusCode ?? "nil status code")
                    
                    if let statusCode = response.response?.statusCode, statusCode == 422 {
                        var errorMessage = ""
                        switch response.result{
                        case .success:
                            if !response.result.value!.message!.isEmpty {
                                errorMessage = response.result.value!.message!
                            } else if !response.result.value!.error!.isEmpty {
                                errorMessage = response.result.value!.error!
                            }else {
                                errorMessage = "errorServer".localized
                            }
                            break
                        case .failure( _):
                            errorMessage = "errorServer".localized
                            break
                        }
                        
                        let error:NSError = NSError(domain: GlobalConstants.errorDomain, code: statusCode, userInfo: ["message":errorMessage])
                        withCompletionBlock(nil, error)
                        return
                    } else if let statusCode = response.response?.statusCode, statusCode == 401 {
                        if(self.tokenRefreshBool){
                            let error:NSError = NSError(domain: GlobalConstants.errorDomain, code: statusCode, userInfo: ["message":"tokenExpired".localized])
                        
                            print("go to refresh Attempts \(self.refresh)")
                            self.refreshToken(isArray: false, statuCode: statusCode, parameters: parameters, service: service, withCompletionBlock: withCompletionBlock)
                            return
                        }
                    }
                    
                    switch response.result{
                    case .success:
                        let code = response.result.value!.code
                        
                        if code! <= 299{
                            withCompletionBlock(response.result.value,nil)
                        }else{
                            var entity : AnyObject = "" as AnyObject
                            if let entityError = response.result.value{
                                entity = entityError
                            }
                            var errorMessage = ""
                            if !response.result.value!.message!.isEmpty {
                                errorMessage = response.result.value!.message!
                            } else {
                                if let resultErrorValue = response.result.value?.error{
                                    errorMessage = resultErrorValue
                                }else{
                                    errorMessage = "errorServer".localized
                                }
                            }
                            let error:NSError = NSError(domain: GlobalConstants.errorDomain, code: code!, userInfo: ["message":errorMessage, "entity" : entity])
                            withCompletionBlock(nil,error)
                        }
                        break
                        
                    case .failure(let error):
                        var messageResponse = "errorServer".localized
                        let code : Int?
                        if response.response?.statusCode != nil{
                            code = response.response!.statusCode
                        }else{
                            if let err = error as? URLError, err.code  == URLError.Code.notConnectedToInternet{
                                print("noInternetConection".localized.uppercased())
                                NotificationCenter.default.post(name: .noConections, object: nil)
                            }
                            code = 500
                        }
                        
                        print(response.result.error!.localizedDescription)
                        
                        let error:NSError = NSError(domain: GlobalConstants.errorDomain, code: code!, userInfo: ["message": messageResponse])
                        
                        withCompletionBlock(nil,error)
                        break
                    }//Switch
            }//Alamofire
        })//SelectWebService
    }//Function
    
    func callServiceObjectArray(parameters:[String: AnyObject]?,service:String, withCompletionBlock: @escaping ((AnyObject?, _ error: NSError?) -> Void)){
        
        selectWebService(service: service, params:parameters,  returnService: { (method, url, typeEncoding) -> Void in
            
            let headers = settingHeader()
            let parametersEdited = self.editParameters(parameters: parameters)
            var arrayParameters:[AnyObject] = []
            
            print("----------API SERVICE-------------")
            print(url ?? "nil")
            print(parametersEdited ?? "nil params")
            print(headers ?? "nil headers")
            print("----------------------------------")
            
            var request = URLRequest(url: URL(string: url!)!)
            request.httpMethod = method.rawValue
            request.allHTTPHeaderFields = headers
            
            
            if parameters != nil{
                request.httpBody = try! JSONSerialization.data(withJSONObject: parametersEdited!)
            }
            
            if ((parameters?.index(forKey: "data")) != nil){
                
                for myValue in (parameters?.values)! {
                    arrayParameters.append(myValue)
                }
                request.httpBody = try! JSONSerialization.data(withJSONObject: arrayParameters[0])
            }
            
            print(arrayParameters)
            //Alamofire.request(url!, method: method, parameters: parametersEdited, encoding: typeEncoding, headers:headers)
            
            Alamofire.request(request)
                .validate(statusCode: 200..<500)
                .responseJSON { response in
                    print(response)
                    
                }
                .responseObject{ (response: DataResponse<JSONResponse>) in
                    print(response.response?.statusCode ?? "nil status code")
                    
                    if let statusCode = response.response?.statusCode, statusCode == 422 {
                        
                        var errorMessage = ""
                        switch response.result{
                        case .success:
                            if !response.result.value!.message!.isEmpty {
                                errorMessage = response.result.value!.message!
                            } else if !response.result.value!.error!.isEmpty {
                                errorMessage = response.result.value!.error!
                            }else {
                                errorMessage = "errorServer".localized
                            }
                            break
                        case .failure( _):
                                errorMessage = "errorServer".localized
                                break
                        }
                        
                        let error:NSError = NSError(domain: GlobalConstants.errorDomain, code: statusCode, userInfo: ["message":errorMessage])
                        withCompletionBlock(nil, error)
                        return
                    } else if let statusCode = response.response?.statusCode, statusCode == 401 {
                        if(self.tokenRefreshBool){
                            let error:NSError = NSError(domain: GlobalConstants.errorDomain, code: statusCode, userInfo: ["message":"tokenExpired".localized])

                            print("go to refresh Attempts \(self.refresh)")
                            self.refreshToken(isArray: true, statuCode: statusCode, parameters: parameters, service: service, withCompletionBlock: withCompletionBlock)
                            return
                        }
                    }
                    
                    switch response.result{
                    case .success:
                        let code = response.result.value!.code
                        
                        if code! <= 299{
                            withCompletionBlock(response.result.value,nil)
                        }else{
                            var entity : AnyObject = "" as AnyObject
                            if let entityError = response.result.value{
                                entity = entityError
                            }
                            var errorMessage = ""
                            if !response.result.value!.message!.isEmpty {
                                errorMessage = response.result.value!.message!
                            } else {
                                errorMessage = response.result.value!.error!
                            }
                            let error:NSError = NSError(domain: GlobalConstants.errorDomain, code: code!, userInfo: ["message":errorMessage, "entity" : entity])
                            withCompletionBlock(nil,error)
                        }
                        break
                        
                    case .failure(let error):
                        var messageResponse = "errorServer".localized
                        let code : Int?
                        if response.response?.statusCode != nil{
                            code = response.response!.statusCode
                        }else{
                            if let err = error as? URLError, err.code  == URLError.Code.notConnectedToInternet{
                                print("noInternetConection".localized.uppercased())
                                NotificationCenter.default.post(name: .noConections, object: nil)
                            }
                            code = 500
                        }
                        
                        print(response.result.error!.localizedDescription)
                        
                        let error:NSError = NSError(domain: GlobalConstants.errorDomain, code: code!, userInfo: ["message": messageResponse])
                        
                        withCompletionBlock(nil,error)
                        break
                    }//Switch
            }//Alamofire
        })//SelectWebService
    }//Function
    
    
    struct JSONArrayEncoding: ParameterEncoding {
        private let array: [Parameters]
        
        init(array: [Parameters]) {
            self.array = array
        }
        
        func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
            var urlRequest = try urlRequest.asURLRequest()
            
            let data = try JSONSerialization.data(withJSONObject: array, options: [])
            
            if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
                urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            }
            
            urlRequest.httpBody = data
            
            return urlRequest
        }
    }
}
