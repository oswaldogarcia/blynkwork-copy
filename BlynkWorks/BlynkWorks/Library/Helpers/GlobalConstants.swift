//
//  GlobalConstants.swift
//  BlynkWorks
//
//  Created by Shirley on 19/2/18.
//  Copyright © 2018 Teravision. All rights reserved.
//

import Foundation
import UIKit

import Foundation
import UIKit
var apiUrl = ""
var filesUrl = ""

//MARK: Loading Configuration Details
enum Environment: String {
    case development = "development"
    case qa = "qa"
    case staging = "staging"
    case production = "production"
    
    private struct Domains {
        static let development = "http://74.208.205.2:10010"
        static let qa = "http://74.208.205.2:10012"
        static let staging = "http://74.208.205.2:10011"
        static let production = "http://blynkwork.com"
    }
    
    private  struct Routes {
        static let api = "/api/v1/"
        static let files = "/blynkworks/public"
    }
    
    var baseUrl: String {
        switch self {
        case .development:
            return setUrl(domain: Domains.development,
                          route: Routes.api,
                          routeType:"API").apiUrl
        case .qa:
            return setUrl(domain: Domains.qa,
                          route: Routes.api,
                          routeType:"API").apiUrl
        case .staging:
            return setUrl(domain: Domains.staging,
                          route: Routes.api,
                          routeType:"API").apiUrl
        case .production:
            return setUrl(domain: Domains.production,
                          route: Routes.api,
                          routeType:"API").apiUrl
        }
    }
    
    var filesUrl: String {
        switch self {
        case .development:
            return setUrl(domain: Domains.development,
                          route: Routes.files,
                          routeType:"FILE").filesUrl
        case .qa:
            return setUrl(domain: Domains.qa,
                          route: Routes.files,
                          routeType:"FILE").filesUrl
        case .staging:
            return setUrl(domain: Domains.staging,
                          route: Routes.files,
                          routeType:"FILE").filesUrl
        case .production:
            return setUrl(domain: Domains.production, route: Routes.files, routeType:"FILE").filesUrl
        }
    }
    
    var token: String {
        switch self {
        case .development: return "ddtopir156dsq16sbi8"
        case .qa: return "qatopir156dsq16sbi8"
        case .staging: return "statopir156dsq16sbi8"
        case .production: return "prod5zdsegr16ipsbi1lktp"
        }
    }
    
}

func setUrl(domain:String, route:String, routeType:String)->(apiUrl: String, filesUrl: String){
    routeType == "API" ? (apiUrl = domain + route) : (filesUrl = domain + route)
    return (apiUrl, filesUrl)
}

//MARK: Parse the configuration name and initialize
struct Configuration {
    var environment: Environment = {
        if let configuration = Bundle.main.object(forInfoDictionaryKey: "Configuration") as? String {
            if configuration.range(of:"QA") != nil {
                return Environment.qa
            }else if configuration.range(of:"Staging") != nil {
                return Environment.staging
            }else if configuration.range(of:"Production") != nil {
                return Environment.production
            }
        }
        return Environment.development
    }()
}

struct GlobalConstants {
    
    //MARK: CONSTANTS
    static let errorDomain = "blynkworks.error"
    static let appVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    static let iosVersion = UIDevice.current.systemVersion
    static let lang =  (NSLocale.preferredLanguages[0] as NSString).substring(to: 2)
    //MARK: Configuring the environment
    private static let baseUrl = apiUrl
    private static let filesRoute = filesUrl
    
    //MARK: ENDPOINTS
    struct Endpoints {
        static let accountRegisterWS = baseUrl+"account/register"
    }
    
    //MARK: ENVIRONMENTS
    struct Development { //DEVELOPMENT
        static let clientId = "devClientId"
        static let clientSecret = "devClientSecret"
        static let apiVersionHeader = "application/x.soma.v"+appVersion+"+json"
        
    }
    
    struct Integration { //INTEGRATION
        static let clientId = "intClientId"
        static let clientSecret = "intClientSecret"
        static let apiVersionHeader = "application/x.blynkworks.v"+appVersion+"+json"
    }
    
    struct QA { //QA
        static let clientId = "qaClientId"
        static let clientSecret = "qaClientSecret"
        static let apiVersionHeader = "application/x.blynkworks.v"+appVersion+"+json"
    }
    
    struct Production { //PRODUCTION
        static let clientId = "prodClientId"
        static let clientSecret = "prodClientSecret"
        static let apiVersionHeader = "application/x.blynkworks.v"+appVersion+"+json"
    }
    
    struct Headers {
        static let contentType = "application/json"
    }
    
    //MARK: DEVICES SIZES
    struct Device {
        static let screenWidth = UIScreen.main.bounds.width
        static let screenHeight = UIScreen.main.bounds.height
        static let isIphone4orLess = (screenHeight < 568.0)
        static let isIphone5 = (screenHeight == 568.0)
        static let isIphone6 = (screenHeight == 667.0)
        static let isIphone6Plus = (screenHeight == 736)
        static let isIphone7 = (screenHeight == 667.0)
        static let isIphone7Plus = (screenHeight == 736)
        static let isIphone8 = (screenHeight == 812.0)
        static let isIphoneX = (screenHeight == 812.0)
        static let isIphoneXOrMore = (screenHeight >= 812.0)
        static let asDefault = "simulator"
        
        static let iPhone5Height = 568.0
        static let iPhone7Height = 667.0
        static let iPhone7PlusHeight = 736.0
        static let iPhoneXHeight = 812.0
        
        static let iPhone5Width:CGFloat = 320.0
        static let iPhone7Width:CGFloat = 375.0
        static let iPhone7PlusWidth:CGFloat = 414.0
        
    }
    
    //MARK: Colors
    struct Colors {
        
        static let fontColor:UIColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)

    }
    
    struct Keys{
        static let sessionUser = "sessionUser"
    }
    
    struct DateFormat {
        
        static let english = "MM/dd/yyyy"
        static let spanish = "dd/MM/yyyy"
        static let englishTime24 = "MM/dd/yyyy HH:mm"
        static let englishTime = "MM/dd/yyyy h:mm a"
        static let spanishTime24 = "dd/MM/yyyy HH:mm"
        static let spanishTime = "dd/MM/yyyy h:mm a"
        static let time12Appt = "EEE, dd MMMM h:mm a"
        static let time24Appt = "EEE, dd MMMM HH:mm"
        static let orderFormat = "yyyy/MM/dd"
        static let serviceFormat = "YYYY-MM-dd"
        static let localeFormat = "en_US_POSIX"
        static let apiFormat = "yyyy-MM-dd HH:mm:ss"
        static let fromService = "dd/MM/yyyy - HH:mm"
        static let fromServicelocale = "dd/MM/yyyy - hh:mm a"
        static let dateZ = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    }
    
    struct TimezoneFormat {
        
        static let UTCTimezone = "UTC"
    }
    
    struct clientCredentials {
        static let client_id = ""
        static let client_secret = ""
        static let operativeSystem = "ios"
    }
    
    struct nameServices {
        static let accountRegister = "AccountRegisterWS"
    }
    
    struct statusServices {
        static let publicService = "public"
        static let privateService = "private"
    }
    
}
