//
//  ParentRegisterController.swift
//  BlynkWorks
//
//  Created by Oswaldo Garcia on 21/02/18.
//  Copyright © 2018 Teravision. All rights reserved.
//

import UIKit


class ParentRegisterController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var parentsTable: UITableView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    
    var parentsCount = 1
    

    override func viewDidLoad() {
        super.viewDidLoad()
        let dateformatter = DateFormatter()
        dateformatter.dateStyle = .full
        dateLabel.text = dateformatter.string(from: Date())
    }
    
    @IBAction func addParents(_ sender: Any) {
        
        self.parentsCount = parentsCount + 1
        self.parentsTable.reloadData()
        let indexPath = IndexPath.init(row: self.parentsCount-1, section: 0)
        self.parentsTable.scrollToRow(at:indexPath , at: .bottom, animated: true)
       
    }
    
    @IBAction func selectDateAction(_ sender: Any) {
    }
    
    @IBAction func selectLocationAction(_ sender: Any) {
    }
    
    
    
    
    
    @IBAction func cancelAction(_ sender: Any) {
    }
    @IBAction func nextAction(_ sender: Any) {
    }
    
    
    
    // MARK: - Table DataSource methods
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return parentsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ParentCell") as! ParentCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return parentsTable.frame.size.height;
    }
    
    

}
