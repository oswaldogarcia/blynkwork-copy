//
//  CalendarController.swift
//  BlynkWorks
//
//  Created by Oswaldo Garcia on 21/02/18.
//  Copyright © 2018 Teravision. All rights reserved.
//

import UIKit
import FSCalendar


class CalendarController: UIViewController , FSCalendarDataSource, FSCalendarDelegate {
    
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        let dateformatter = DateFormatter()
        dateformatter.dateStyle = .full        
        dateLabel.text = dateformatter.string(from: Date())
       
     }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
      
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
        }
        
        let dateformatter = DateFormatter()
        
        dateformatter.dateStyle = .full
        
        dateLabel.text = dateformatter.string(from: date)
        
    }
    
    @IBAction func closeCalendar(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
}

