//
//  ParentCell.swift
//  BlynkWorks
//
//  Created by Oswaldo Garcia on 21/02/18.
//  Copyright © 2018 Teravision. All rights reserved.
//

import UIKit

class ParentCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
